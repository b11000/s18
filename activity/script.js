// Activity:
// 1. In the S18 folder, create an activity folder and an index.html and script.js file inside of it.
// 2. Link the script.js file to the index.html file.
// 3. Create a trainer object using object literals.
// 4. Initialize/add the following trainer object properties:
// - Name (String)
// - Age (Number)
// - Pokemon (Array)
// - Friends (Object with Array values for properties)
// 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
// 6. Access the trainer object properties using dot and square bracket notation.
// 7. Invoke/call the trainer talk object method.

let pkmnTrainer ={
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["CHARMANDER","RATATA","CATERPIE",],
	friends: {	hoenn: ["May","Max"],
				kanto: ["Brock","Misty"]
	},
	talk: function(){
		return `${this.pokemon[0]}! I choose you!`
	},
	talk2: function(){
		return `CHAR CHARMANDER!`
	}
}

console.log(pkmnTrainer);
console.log(`The Result of dot notation: `);
console.log(pkmnTrainer.name);
console.log(`The Result of square bracket notation: `);
console.log(pkmnTrainer['pokemon']);
console.log(`The Result of talk method`);
console.log(pkmnTrainer.talk());
console.log(pkmnTrainer.talk2());
console.log(`                         `)
console.log(`----END OF TESTING!!!----`)
console.log(`                         `)
console.log(`----END OF TESTING!!!----`)
console.log(`                         `)
console.log(`----END OF TESTING!!!----`)
console.log(`                         `)

// 8. Create a constructor for creating a pokemon with the following properties:
// - Name (Provided as an argument to the contructor)
// - Level (Provided as an argument to the contructor)
// - Health (Create an equation that uses the level property)
// - Attack (Create an equation that uses the level property)
// 
// 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.

// 10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
// 11. Create a faint method that will print out a message of targetPokemon has fainted.
// 
// 12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
// 13. Invoke the tackle method of one pokemon object to see if it works as intended.

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 1.5 * level;
	this.attack = .45 * level;
	this.specialattack = 1.35 * level;

	this.scratch = function(target){
		console.log(`${this.name} use SCRATCH!`)
		let health = target.health - this.attack;

		this.faint = function(target){
		console.log(`${target.name} fainted!`)
		}
		if(health < 0){
			this.faint(target);
		} else {
			console.log(`${target.name}!'s health is now reduce to ${health}`)
			return (target.health = health);
			}
	}
	this.tackle = function(target){
		console.log(`${this.name} use TACKLE!`)
		let health = target.health - this.specialattack;

		this.faint = function(target){
		console.log(`${target.name} fainted!`)
		}
		if(health < 0){
			this.faint(target);
		} else {
			console.log(`${target.name}!'s health is now reduce to ${health}`)
			return (target.health = health);
			}
	}
}

let charmander = new Pokemon('CHARMANDER', 6)
let charmander2 = new Pokemon('CHARMANDER', 7)
let ratata = new Pokemon('RATATA', 4)
let caterpie = new Pokemon('CATERPIE', 2)

let pidgey = new Pokemon('WILD PIDGEY', 3)

console.log(`Wild PIDGEY appeared!`);
console.log(`Go CYNDAQUIL!`);
console.log(`What will CHARMANDER do?`);
console.log(`>FIGHT	POKéMON		BAG 	RUN`);
console.log(`FIGHT	>POKéMON		BAG 	RUN`);
console.log(`>CHARMANDER	RATATA	CATERPIE	`);
console.log(charmander);
console.log(`CHARMANDER	>RATATA	CATERPIE	`);
console.log(ratata);
console.log(`CHARMANDER	RATATA	>CATERPIE	`);
console.log(caterpie);
console.log(`CHARMANDER, That's enough! Come back!`);
console.log(`Go CATERPIE!`);
pidgey.tackle(caterpie);
console.log(`Use next POKéMON? 	>YES	NO`);
console.log(`Go CYNDAQUIL!`);
console.log(`>SCRATCH	GROWL	- 		-`);
charmander.scratch(pidgey)
pidgey.scratch(charmander)
charmander.scratch(pidgey)
console.log(`CHARMANDER gained 23 EXP. Points!`);
console.log(`CHARMANDER grew to LV.7!`);
console.log(charmander2);
console.log(`CHARMANDER learned EMBER!`);
console.log(`                         `)
console.log(`POKéDEX`)
console.log(`POKéMON`)
console.log(`BAG`)
console.log(`ASH`)
console.log(`>SAVE`)
console.log(`OPTION`)
console.log(`EXIT`)
console.log(`                         `)
console.log(`PLAYER		ASH`)
console.log(`BADGES		0`)
console.log(`POKéDEX		ASH		>YES`)
console.log(`TIME		5:30	 NO`)
console.log(`Would you like to save the game?`)
console.log(`                         `)
console.log(`                    >YES     `)
console.log(`                     NO `)
console.log(`There is already a saved file.`)
console.log(`Is it okay to overwrite it?`)
console.log(`                         `)
console.log(`SAVING... `)
console.log(`DONT TURN OFF THE POWER.`)
console.log(`ASH saved the game.`)




// 14. Create a git repository named S18.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code."S18-Activity"
// 16. Add the link in Boodle. "Javascript - Objects"