// Objects 
// A collection/ of related data

let grades = [98.5, 94.3, 89.2, 90.1];

console.log(grades[0])

// An object is similar to an array. It is a collection of related data and /or functionalities. usually it represent real world objects.

console.log(grades)

let grade = {
	// object initializer/literal notation - Objects consist of properties, which are used to describe an object. Key-value pair
	math: 89.2,
	english: 98.5,
	science: 90.1,
	filipino: 94.3
}
// we used dot notation to access the value of our object
console.log(grade.english)

// Syntax:
/*
	let objectName={
		keyA: valueA,
		keyB: valueB
	}
*/

let cellphone = {
	brandName: 'Nokia3310',
	color: 'DarkBlue',
	manufacturerDate: 1999
}

console.log(typeof cellphone)

let student = {
	firstName: 'John',
	lastName: 'Smith',
	mobileNumber: '091234567890',
	location: {
		city: 'Tokyo',
		country: 'Japan'
	},
	emails:['john@mail.com', 'johnsmith@mail.xyz'],
	// object method(function inside an object)
	fullName: function(){
		return this.firstName + ' ' + this.lastName
	}

}
// dot notation 
console.log(student.location.city)
// bracket notation
console.log(student['firstName'])

console.log(student.emails)
console.log(student.emails[0])
console.log(student.fullName())

// sample for 'this'
const person1 = {
	name: 'Jane',
	greeting: function(){
		return `Hi I\'m ` + this.name
	}
}

console.log(person1.greeting())

// ARRAY OF OBJECTS
let contactList = [
	{
		firstName: 'John',
		lastName: "Smith",
		location: 'Japan'	
	},
	{
		firstName: 'Jane',
		lastName: "Smith",
		location: 'Japan'	
	},
	{
		firstName: 'Jasmine',
		lastName: "Smith",
		location: 'Japan'	
	},

]
console.log(contactList[0].firstName)

let people = [
	{
		name: 'Juanita',
		age: 13
	},
	{
		name: 'Juanito',
		age: 14
	}
]

people.forEach(function(person){
	console.log
})

// console.log(`${people[].name} are the list`)

// Creating objects using a Constructor Function (JS Object Constructors/Object From Blueprints)

// Creates a reusable to function to create several objects that have the same data structure
// This is useful for creating multiple copies/instances of an object
// Object Literals
// let object = {}
// Instance - It has distinct/unique objects
// let object = new object
// An instance is a concrete occurence of any object which emphasizes on the unique identity of it
/*
	Syntax:
	function ObjectName(keyA, keyB){
		this.keyA = keyA,
		this.keyB = keyB
	}
 */
function Laptop(name, manufactureDate){
	// The 'this' keyword allows to assign a new object's property by associating them with values received from our parameter
	this.name = name,
	this.manufactureDate = manufactureDate
}

// This is a unique instance of the Laptop object

let laptop = new Laptop('Lenovo', 2008)

console.log(`Result from creating objects using object constructors`)
console.log(laptop)

// This is another unique instance of the laptop object
let myLaptop = new Laptop('Macbook Air', 2020)
console.log('Result from creating objects using object constructors')
console.log(myLaptop)

let oldLaptop = Laptop
console.log(`without new keyword`)
console.log(oldLaptop)

// Creating empty objects
let computer = {}
let myComputer = new Object();


// name of the Macbook using the dot notation and bracket notation

console.log(myLaptop.name)
console.log(myLaptop['name'])

let array = [laptop, myLaptop]

console.log(array[0].name)

// Initializing/Adding/Deleting/Reassigning Object Properties

// initialized/added properties after the object was created
// This is useful for times when an object's properties are undetermined at the time of creating them

let car = {}

// add an object properties, we can use dot notation

// dot Notation
car.name = 'Honda Civic'
console.log(car)
// bracket Notation
car['manufactureDate'] = 2019;
console.log(car)

// Reassignning object properties
car.name = 'Dodge Charger R/T'
console.log(car)

// Deleting object properties
delete car['manufactureDate']
console.log(`Result from deleting properties`)
console.log(car)

// Deleting object properties using dot Notation
// delete car.name
// console.log(`Result from deleting properties`)
// console.log(car)

// Object Methods
// A method is a function which is a property of an object
// They are also functions and one of the key differences they have is that methods are functions related to a specific object

// Object Literals
let person ={
	name: 'John',
	talk: function() {
		console.log('Hello my name is ' + this.name)
	}
}

console.log(person)
console.log('Result from object methods')
person.talk()

// Adding methods to object person
person.walk = function() {
	console.log(this.name + ' walked 25 steps forward')
}

person.walk()

// Object Methods
let friend = {
	firstName : 'Joe',
	lastName: 'Smith',
	address:{
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@mail.xyz'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName)
	}
}
friend.introduce()

/*
Real World Application of Objects
-Scenario
	1. We would like to create a game that would have several pokemon interact with each other
	2. Every pokemon would have the same set of stats, properties and functions
 */

// Using object literals to create multiple kinds of pokemon would be time consuming

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log('This Pokemon tackled target Pokemon')
		console.log("'targetPokemon's health is now reduce to _targetPokemonHealth_")
	},
	faint: function(){
		console.log('Pokemon fainted')
	}
}

// Creating an object constructor instead of objects literals
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name)
		console.log("targetPokemon's health is now reduce to _targetPokemonHealth_")
	}
	this.faint = function(){
		console.log(this.name + 'fainted.')
	}
}
let pikachu = new Pokemon('Pikachu', 16)
let charizard = new Pokemon('Charizard', 8)

pikachu.tackle(charizard)

